import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.mail.internet.MimeMessage;

import com.hc.email.api.EmailException;
import com.hc.email.api.EmailService;
import com.hc.email.api.MimeMessageFactory;
import com.hc.email.gmail.GmailService;

public class GmailQuickstart {
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";
	
    public static void main(String... args) throws EmailException, FileNotFoundException {
    	new GmailQuickstart().execute();
    }
    
    public void execute() throws EmailException, FileNotFoundException {
    	EmailService emailService = GmailService.of(loadCredentials());
    	
    	MimeMessage message = MimeMessageFactory.create("henriquecalhau0608@gmail.com","Subject it is", "Bodytext here...");
    	emailService.sendEmail(message);
    }
    
    private InputStream loadCredentials() throws FileNotFoundException {
		// Load client secrets.
		InputStream in = GmailQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		
		return in;
    }
}
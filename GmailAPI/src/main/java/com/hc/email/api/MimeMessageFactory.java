package com.hc.email.api;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MimeMessageFactory {
	private static final String ME = "me";

	private MimeMessageFactory() {
	}
	
    /**
     * Create a MimeMessage using the parameters provided.
     *
     * @param to email address of the receiver
     * @param from email address of the sender, the mailbox account
     * @param subject subject of the email
     * @param bodyText body text of the email
     * @return the MimeMessage to be used to send email
     * @throws EmailException with the nested MessagingException
     */
	public static MimeMessage create(String to, String subject, String bodyText) throws EmailException {
        MimeMessage email = new MimeMessage(Session.getDefaultInstance(new Properties(), null));
        try {
			email.setFrom(new InternetAddress(ME));
	        email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));
	        email.setSubject(subject);
	        email.setText(bodyText);
		} catch (MessagingException e) {
			throw new EmailException(e);
		} 
        
        return email;
    }
}

package com.hc.email.api;

import javax.mail.internet.MimeMessage;

public interface EmailService {
	public void sendEmail(MimeMessage mimemessage) throws EmailException;
}

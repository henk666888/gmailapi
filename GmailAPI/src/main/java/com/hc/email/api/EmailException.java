package com.hc.email.api;

public class EmailException extends Exception {
	private static final long serialVersionUID = 4462935126472341026L;

	public EmailException(Throwable cause) {
		super(cause);
	}

	public EmailException(String message, Throwable cause) {
		super(message, cause);
	}
}

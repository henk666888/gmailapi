package com.hc.email.gmail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Draft;
import com.google.api.services.gmail.model.Message;
import com.hc.email.api.EmailException;
import com.hc.email.api.EmailService;

public class GmailService implements EmailService {
    private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
    
    
    private final Gmail gmailService;
    
    private GmailService(InputStream credentialsInputStream) throws EmailException {
    	try {
    		final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    		
    		Credential credentials = getCredentials(httpTransport, credentialsInputStream);
			gmailService = createGmailService(httpTransport, credentials);
		} catch (GeneralSecurityException | IOException e) {
			throw new EmailException(e);
		}
    }
    
    
    public void createDraft(MimeMessage mimemessage) throws EmailException {
		Draft draft = new Draft();
	    try {
			draft.setMessage(createMessageWithEmail(mimemessage));
			draft = gmailService.users().drafts().create("me", draft).execute();
		} catch (MessagingException | IOException e) {
			throw new EmailException("Error creating gmail draft", e);
		}
    }
    
    @Override
    public void sendEmail(MimeMessage mimemessage) throws EmailException {
    	try {
			gmailService.users().messages().send("me", createMessageWithEmail(mimemessage)).execute();
		} catch (IOException | MessagingException e) {
			throw new EmailException("Error sending gmail message", e);
		}
    }
	
    /**
     * Create a message from an email.
     *
     * @param emailContent Email to be set to raw of message
     * @return a message containing a base64url encoded email
     * @throws IOException
     * @throws MessagingException
     */
    private Message createMessageWithEmail(MimeMessage emailContent) throws MessagingException, IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        
        Message message = new Message();
        message.setRaw(encodedEmail);
        
        return message;
    }

    private Gmail createGmailService(NetHttpTransport httpTransport, Credential credentials) throws GeneralSecurityException, IOException {
        return new Gmail.Builder(httpTransport, JSON_FACTORY, credentials)
                .setApplicationName(APPLICATION_NAME)
                .build();
	}
	
    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport httpTransport, InputStream credentialsInputStream) throws IOException {
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(credentialsInputStream));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
        		httpTransport, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
    
    public static EmailService of(InputStream credentials) throws EmailException {
    	return new GmailService(credentials);
    }
}

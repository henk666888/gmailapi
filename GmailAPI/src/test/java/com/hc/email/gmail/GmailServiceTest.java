package com.hc.email.gmail;

import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.mail.internet.MimeMessage;

import com.hc.email.api.EmailService;
import com.hc.email.api.MimeMessageFactory;
import org.junit.jupiter.api.Test;

public class GmailServiceTest {
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	private EmailService gmailService;

	@Test
	public void sendMessageSuccess() throws Exception {
		// Load client secrets.
		InputStream in = GmailServiceTest.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}

		gmailService = GmailService.of(in);

		MimeMessage message = MimeMessageFactory.create("nmasilva@gmail.com", "Subject it is", "Bodytext here...");

		gmailService.sendEmail(message);
	}

}
